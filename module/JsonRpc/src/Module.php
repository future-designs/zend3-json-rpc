<?php

namespace FuDe\JsonRpc;

/**
 * Class Module
 * @package JsonRpc
 */
class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
