<?php
/**
 * Created by PhpStorm.
 * User: j.lohse
 * Date: 03.11.17
 * Time: 17:52
 */

namespace FuDe\JsonRpc\Controller;


use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class RpcControllerFactory
 * @package FuDe\JsonRpc\Controller
 */
class RpcControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RpcController();
    }
}