FROM centos:latest

MAINTAINER Jacob Lohse <lohse.jacob@googlemail.com>

ENV container docker

# install epel and webtatic repos
RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
&& rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

RUN yum -y update

# install php and apache
RUN yum -y install mod_php71w php71w-opcache php71w-cli php71w-common php71w-gd php71w-intl php71w-mbstring php71w-mcrypt php71w-mysql php71w-mssql php71w-pdo php71w-pear php71w-soap php71w-xml php71w-xmlrpc httpd

RUN rm -rf /etc/localtime \
 && ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# new directory for zend-installations (see v-host config)
#RUN mkdir -p /var/www/page

# copy all configs to docker

#RUN mkdir -p /opt/mydocker/config
WORKDIR /opt/mydocker/config

COPY docker .

# php and httpd configurations

RUN cp php_settings.ini /etc/php.d/ \
 && cp v-host.conf /etc/httpd/conf.d/

# database configuration
#RUN bash /opt/mydocker/config/db/db-config.sh

# composer

RUN curl -sS https://getcomposer.org/installer \
| php -- --install-dir=/usr/local/bin --filename=composer

# clean up
RUN yum clean all

VOLUME ["/var/log/httpd"]

# enable httpd
RUN systemctl enable httpd

WORKDIR /var/www/page

#RUN php /usr/local/bin/composer install

CMD ["/usr/sbin/init"]
